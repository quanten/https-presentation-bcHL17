# Barcamp HTTPS/TLS Session

* Viele Namen dafür
* HTTP Hyper Text Tranport Protocol
* Sichert das ab mit SSL (Secure Sockets Layer) oder TLS (Transport Layer Security)


* Die Verbindung zwischen Client (Handy, Computer) und dem Server wie Zettel in Schulklasse rum gegeben
    * > Bist du wirklich meine Bank? O Ja O Nein O Vielleicht
    * < Ja, kannst dir sicher sein ;-)
    * > Überweise mal 10€ an -Bob- Mallory
    * < An wen denn jetzt? :/
    * Zettel besucht viele Hände und wird gelesen
    * Zettel kann verändert werden
    * Unterschrift fehlt
    

* Anforderungen an sichere Kommunikation
    * Vertraulichkeit
        * Niemand soll mit lesen
            * Private Gespräche
            * Passwörter
            * Nutzerüberwachung
        * nicht jetzt
        * meist auch nicht bald (Forward Security)
            * Informationen müssen nicht immer ewig geheim sein
            * Nutzen vs Kosten vs Zeit zum Cracken
    * Integrität
        * Beide Richtungen sollen unverändert bleiben
            * zB Empfänger von Banküberweisungen
    * Identifikation/Authentizität
        * nur mit dem richtigen Unterhalten
        * ohne sind andere Punkt sinnlos
        
    * Beginnen mit Vertraulichkeit

* Warum wir nicht Cäsar vertrauen und Keyprobleme
    * Symmetrisch, beide Seiten gleicher Key
        * wer Verschlüsselt kann auch entschlüsseln
    * Beispiel Cäsar in der Schule
        * Alphabet um N stellen verschieben (unsicher, häufigster Buchstabe = e)
    * AES (Advanced Encryption Standart)
        * So lange Key sicher und richtig eingebaut, ist es sicher
    * Vorteile:
        * Schnell
        * Einfach zu verstehen
    * Nachteile:
        * Verlagert Sicherheitsproblem in ein Keymanagment Problem
        * Keyaustuasch ist das Problem
            * Brieftaube ist nicht praktisch

* Schlüsselaustausch ohne RSA, sonder mit DH
    * Diffie Hellmann Austausch
    * Gemeinsamen Schlüssel zusammen aushandeln
    * Zuschauen führt nicht zum Key (Aber Manipulieren, daher Integrität wichtig)

* Asymmetrische Verschlüsselung
    * coole Lösung für viele Probleme
    * Asymm = nicht gleicher Key für Verschlüsselung sowie Entschlüsselung
    * Es gibt nun mathematisch verbundene Schlüsselpaare. Ein privater, ein öffentlicher
    * 7 x 13 = 91
    * 221 = 13 x 17
    * Basierend auf Multiplikation von Primzahlen. Faktorisieren schwer

    * Austausch damit
        * mit dem öffentlichen Verschlüsselt, kann nur mit privaten Entschlüsselt werden
        * Key schicken, fertig
        * Im nachhinein bei bekanntwerden der Keys entschlüsselbar, DHs werden nach Session verworfen

    * Integrität (Signing)
        * Auch andersrum möglich
        * mit privaten "Verschlüsselt". Nicht wirklich Verschlüsselt, da mit öffentlichen entschlüsselbar, aber signiert
        * Nur Inhaber des privaten Schlüssels kann das machen

    * Handwerkszeug zusammen
        * Vertraulich Kommunizieren, dank AES (Symmetrisch)
        * Schlüssel Austauschen, dank Diffie Hermann Farbmischung
        * Integrität dank Unterschrift
        * Nun fehlt Identifikation, Will ja nicht von Jeder Seite eine Unterschriftkopie haben müssen

* CAs oder PKI
    * Public Key Infrastrukture
    * Zertifizierungsstellen
    * Stellen Ausweise aus
    * Chain of trust

    * Certs
        * Domain only
            * Nur ob die Auskunft richtig ist
        * EVs
            * Geschäftsadresse
            * Inhaber der Domain (Registar fragen)

    * Macht der Hersteller
        * Stellen CAs ein

    * Symantec
        * falsche CAs ausgestellt

    * Dell Laptops
        * eigenes CA eingebaut
        * privaten Key vergessen
            * online verfügbar
            * Zertifikate austellen die anerkannt werden

* keine Ausreden mehr: Lets Encrypt

* "Security devices"

