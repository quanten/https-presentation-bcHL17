.. title:: HTTPS

:skip-help: true

:css: big_font.css

.. role:: strike
    :class: strike

----

httpS, TLS / SSL
=================

aka "das kleine grüne Schloss"

.. image:: lock.svg
    :height: 100px


----

.. image:: verlauf.png
    :height: 700px

----

Was soll sichere Kommunikation leisten?
---------------------------------------

* Vertraulichkeit

* Integrität

* Identifikation / Authentizität

----

Cäsar-Crypto
============

Symmetrische Verschlüselung
---------------------------

----

Heute eher: AES
===============

Cool:
=====

* Schnell
* Verfahren an sich sicher

Nicht so toll:
==============

* Keymanagment

----

.. image:: DH.svg
    :height: 700px

----


RSA - Zwei Keys
===============

Cool:
=====

* Schlüsselpaar
* Verfahren an sich sicher

Nicht so toll:
==============

* langsam


----

13 * 7 = ?
==========

----

221 = X * Y
===========


----

Schlüsseltausch mit RSA
========================
* Schlüssel mit RSA verschlüsseln
* verschicken

----

Was soll sichere Kommunikation leisten?
---------------------------------------


* :strike:`Vertraulichkeit`

* Integrität

* Identifikation

----

Signieren mit RSA
=================
wie Verschlüsseln nur andersrum
-------------------------------

----

Was soll sichere Kommunikation leisten?
---------------------------------------


* :strike:`Vertraulichkeit`

* :strike:`Integrität`

* Identifikation

----


Identifikation mit RSA
======================

das Zertifikat wird von einer dritten Stelle als Vertrauendswürdig anerkannt

----

Wer sagt mir welchem Zertifikat ich vertrauen kann?
===================================================

Die Root Zertifizierungsstelle
------------------------------

----

.. image:: chain.jpg
    :height: 700px

----


Was soll sichere Kommunikation leisten?
---------------------------------------


* :strike:`Vertraulichkeit`

* :strike:`Integrität`

* :strike:`Identifikation`


----

Symantec
========

Norton Security
---------------

----

So nicht, @Dell
===============

----

.. image:: lets.svg
    :height: 200px
    
    
----

"Security Devices"
==================

Man in the middle
-----------------


----

goo.gl/p7UcqE
=====================

https://gitlab.com/quanten/https-presentation-bcHL17

.. image:: qr.svg
    :height: 500px
